const chalkLog = require('./client/messages');
const SHOW_PACKET = true;

function printPacket(packet) {
    if (SHOW_PACKET) {
        chalkLog.logWarning(JSON.stringify(packet));
    }
}

module.exports = {
    printPacket
}
