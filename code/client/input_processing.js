const emoji = require('./emoji');
const chalkLog = require('./messages');
const crypto = require('crypto');
//
// let ecdh;
// let myPublicKey, secretKey;
const shareVariables = require('./share_variables');

const {
    PACKET,
    ACTION_BROADCAST, COMMAND_BROADCAST,
    MSG, SENDER, ACTION,
    ACTION_LIST, COMMAND_LIST,
    ACTION_QUIT, COMMAND_QUIT,
    REGEX_SEND_PRIVATE_MESSAGE,
    ACTION_SEND,
    DEST,
    COMMAND_CREATE_GROUP,
    ACTION_CREATE_GROUP,
    GROUP,
    COMMAND_JOIN_GROUP,
    ACTION_JOIN_GROUP,
    ACTION_BROADCAST_GROUP,
    REGEX_BROADCAST_MESSAGE_TO_GROUP,
    COMMAND_MEMBERS,
    ACTION_MEMBERS,
    ACTION_MESSAGES,
    COMMAND_MESSAGES,
    ACTION_LIST_GROUPS,
    COMMAND_LIST_GROUPS,
    COMMAND_LEAVE_GROUP,
    ACTION_LEAVE_GROUP,
    ACTION_CREATE_SECURE_CONNECTION1,
    COMMAND_CREATE_SECURE_CONNECTION,
    PUBLIC_KEY,
    ECDH_CURVE_NAME,
    REGEX_ENCRYPTED_MESSAGE,
    CIPHER_ALGORITHM,
    INPUT_ENCODING,
    OUTPUT_ENCODING,
    ACTION_SEND_ENCRYPTED_MESSAGE,
    ENCRYPTED_MESSAGE,
    IV
} = require('../constants');

const {printPacket} = require('../print_packet');

function processInput(socket, input, sender) {
    // Broadcast a message to all connected users: b;{message}
    if (true === input.startsWith(COMMAND_BROADCAST)) {
        broadcastMessage(socket, input, sender);
    }

    // List all connected users: ls;
    else if (true === input.startsWith(COMMAND_LIST)) {
        listConnectedUsers(socket, sender);
    }

    // Quit the chat: q;
    else if (true === input.startsWith(COMMAND_QUIT)) {
        quit(socket, sender);
    }

    // Send a message privately to another user: s;{name};{message}
    else if (true === REGEX_SEND_PRIVATE_MESSAGE.test(input)) {
        sendPrivateMessage(socket, input, sender);
    }

    // Create a group: cg;{group name}
    else if (true === input.startsWith(COMMAND_CREATE_GROUP)) {
        createGroup(socket, input, sender);
    }

    // Join a group: cg;{group name}
    else if (true === input.startsWith(COMMAND_JOIN_GROUP)) {
        joinGroup(socket, input, sender);
    }

    // Broadcast message to a group: bg;{group name};{message}
    else if (true === REGEX_BROADCAST_MESSAGE_TO_GROUP.test(input)) {
        broadcastMessageToGroup(socket, input, sender);
    }

    // List all members in a group: members;{group name}
    else if (true === input.startsWith(COMMAND_MEMBERS)) {
        listMembersInGroup(socket, input, sender);
    }

    // List all exchanged messages in a group: messages;{group name}
    else if (true === input.startsWith(COMMAND_MESSAGES)) {
        listGroupMessages(socket, input, sender);
    }

    // List all existing groups: groups;
    else if (true === input.startsWith(COMMAND_LIST_GROUPS)) {
        listExistingGroups(socket, input, sender);
    }

    // Leave a group: leave;{group name}
    else if (true === input.startsWith(COMMAND_LEAVE_GROUP)) {
        leaveGroup(socket, input, sender);
    }

    // Create a secure connection: ss;{user name}
    else if (true === input.startsWith(COMMAND_CREATE_SECURE_CONNECTION)) {
        createSecureConnection(socket, input, sender);
    }

    // Send private encrypted message: sss;{user name};{message}
    else if (true === REGEX_ENCRYPTED_MESSAGE.test(input)) {
        sendEncryptedMessage(socket, input, sender);
    }

    /**
     * If user types non-existing command, we just need to show a warning!
     */
    else {
        const msg = `\n${emoji.warning}  Command not found! Please double check your command!`;
        chalkLog.logWarning(msg);
    }
}

/**
 * Broadcast a message to all other clients: b;{message}
 */
function broadcastMessage(socket, input, sender) {
    const str = input.slice(2);
    if (str && str.trim().length > 0) {
        const packet = {
            [SENDER]: sender,
            [ACTION]: ACTION_BROADCAST,
            [MSG]: str.trim()
        };

        printPacket(packet);

        socket.emit(PACKET, packet);
    }
}

/**
 * List connected users: ls;
 */
function listConnectedUsers(socket, sender) {
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_LIST,
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * Quit the chat: q;
 */
function quit(socket, sender) {
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_QUIT,
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * Send private message to someone: s;{receiver name};message
 */
function sendPrivateMessage(socket, input, sender) {
    const command = input.match(REGEX_SEND_PRIVATE_MESSAGE);
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_SEND,
        [DEST]: command[1], // Receiver name
        [MSG]: command[2] // Message
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * Create a group: cg;{group name}
 * @param socket
 * @param input
 * @param sender
 */
function createGroup(socket, input, sender) {
    const groupName = input.slice(3);
    if (groupName && groupName.length > 0) {
        // {"sender": "sender name", "action": "cgroup", "group": "group name"}
        const packet = {
            [SENDER]: sender,
            [ACTION]: ACTION_CREATE_GROUP,
            [GROUP]: groupName
        };

        printPacket(packet);

        socket.emit(PACKET, packet);
    } else {
        const msg = `\n${emoji.warning}  Please provide group name in your command!`;
        chalkLog.logWarning(msg);
    }
}

/**
 * Join a group: j;{group name}
 * @param socket
 * @param input
 * @param sender
 */
function joinGroup(socket, input, sender) {
    const groupName = input.slice(2);
    if (groupName && groupName.length > 0) {
        // {"sender": "sender name", "action": "jgroup", "group": "group name"}
        const packet = {
            [SENDER]: sender,
            [ACTION]: ACTION_JOIN_GROUP,
            [GROUP]: groupName
        };

        printPacket(packet);

        socket.emit(PACKET, packet);
    } else {
        const msg = `\n${emoji.warning}  Please provide group name in your command!`;
        chalkLog.logWarning(msg);
    }
}

/**
 * Broadcast message to a group: bg;{group name};{message}
 * @param socket
 * @param input
 * @param sender
 */
function broadcastMessageToGroup(socket, input, sender) {
    const command = input.match(REGEX_BROADCAST_MESSAGE_TO_GROUP);
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_BROADCAST_GROUP,
        [GROUP]: command[1], // Receiver name
        [MSG]: command[2] // Message
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * List all members in a group: members;{group name}
 * @param socket
 * @param input
 * @param sender
 */
function listMembersInGroup(socket, input, sender) {
    const group = input.slice(8);
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_MEMBERS,
        [GROUP]: group
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * List all messages in a group.
 * @param socket
 * @param input
 * @param sender
 */
function listGroupMessages(socket, input, sender) {
    const group = input.slice(9);
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_MESSAGES,
        [GROUP]: group
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * List all existing groups: groups;
 * @param socket
 * @param input
 * @param sender
 */
function listExistingGroups(socket, input, sender) {
    const packet = {
      [SENDER]: sender,
      [ACTION]: ACTION_LIST_GROUPS
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * Leave a group: leave;{group name}
 * @param socket
 * @param input
 * @param sender
 */
function leaveGroup(socket, input, sender) {
    const group = input.slice(6);
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_LEAVE_GROUP,
        [GROUP]: group
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * Create secure connection between 2 users
 * @param socket
 * @param input
 * @param sender
 */
function createSecureConnection(socket, input, sender) {
    const receiver = input.slice(3);

    shareVariables.ecdh = crypto.createECDH(ECDH_CURVE_NAME);

    shareVariables.myPublicKey = shareVariables.ecdh.generateKeys();

    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_CREATE_SECURE_CONNECTION1,
        [DEST]: receiver,
        [PUBLIC_KEY]: shareVariables.myPublicKey
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * Send encrypted message
 * @param socket
 * @param input
 * @param sender
 */
function sendEncryptedMessage(socket, input, sender) {
    const command = input.match(REGEX_ENCRYPTED_MESSAGE);

    /**
     * Encrypt message process
     */
    const msg = command[2];
    const randomIV = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(CIPHER_ALGORITHM, shareVariables.secretKey, randomIV);
    let encryptedMessage = cipher.update(msg, INPUT_ENCODING, OUTPUT_ENCODING);
    encryptedMessage += cipher.final(OUTPUT_ENCODING);

    const receiver = command[1];
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_SEND_ENCRYPTED_MESSAGE,
        [DEST]: receiver,
        [ENCRYPTED_MESSAGE]: encryptedMessage,
        [IV]: randomIV
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

module.exports = {
    processInput
}
