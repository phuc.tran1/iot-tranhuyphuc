const chalk = require('chalk');

/**
 * Beautify the client UI using chalk library: https://github.com/chalk/chalk
 * chalk.<style>[.<style>...](string, [string...])
 * Example: chalk.red.bold.underline('Hello', 'world');
 */

function logDefaultMessage(message) {
    console.log(message);
}

function logError(error) {
    console.log(chalk.red.bold(error));
}

function logWarning(warning) {
    console.log(chalk.yellow.bold(warning));
}

function logNewMessage(message) {
    console.log(chalk.green(message));
}

function logItalicMessage(message) {
    console.log(chalk.italic(message));
}


module.exports = {
    logDefaultMessage,
    logWarning,
    logError,
    logItalicMessage,
    logNewMessage
}
