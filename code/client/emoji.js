// https://raw.githubusercontent.com/omnidan/node-emoji/master/lib/emoji.json
const emoji = require('node-emoji');

const greeting = emoji.get('hand');
const pointRight = emoji.get('point_right');
const x = emoji.get('x');
const person = emoji.get('bearded_person');
const sound = emoji.get('loud_sound');
const group = emoji.get('man-man-girl-girl');
const warning = emoji.get('warning');
const bubble = emoji.get('left_speech_bubble');
const snowflake = emoji.get('snowflake');

module.exports = {
    greeting,
    pointRight,
    x,
    person,
    sound,
    group,
    warning,
    bubble,
    snowflake
}
