const io = require('socket.io-client');
const socket = io.connect('http://localhost:3000');
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const {
    PACKET, CONNECT, DISCONNECT, ACTION_JOIN,
    ACTION_BROADCAST, SENDER, ACTION,
    ACTION_LIST,
    ACTION_QUIT,
    ACTION_SEND,
    ACTION_CREATE_GROUP,
    ACTION_JOIN_GROUP,
    ACTION_BROADCAST_GROUP,
    ACTION_MEMBERS,
    ACTION_MESSAGES,
    ACTION_LIST_GROUPS,
    ACTION_LEAVE_GROUP,
    PASSWORD,
    ACTION_CREATE_SECURE_CONNECTION1,
    ACTION_CREATE_SECURE_CONNECTION2,
    ACTION_SEND_PUBLIC_KEY,
    DEST,
    PUBLIC_KEY,
    ECDH_CURVE_NAME,
    CIPHER_ALGORITHM,
    OUTPUT_ENCODING,
    INPUT_ENCODING,
    ACTION_SEND_ENCRYPTED_MESSAGE
} = require('../constants');

const chalkLog = require('./messages');
const emoji = require('./emoji');
const {processInput} = require("./input_processing");
let sender;
let password;
// let ecdh;
// let myPublicKey, partnerPublicKey, secretKey;

const shareVariables = require('./share_variables');

const crypto = require('crypto');

const {printPacket} = require('../print_packet');

socket.on(CONNECT, () => {
    sender = process.argv[2];
    password = process.argv[3];

    const msg = `\n${emoji.greeting} Hello ${sender}`;
    chalkLog.logNewMessage(msg);

    const packet = {
        [SENDER]: sender,
        [PASSWORD]: password,
        [ACTION]: ACTION_JOIN
    };

    printPacket(packet);

    socket.emit(PACKET, packet)
});

/**
 * Listen to the disconnect event from server.
 */
socket.on(DISCONNECT, (reason) => {
    const msg = `\n${emoji.x} Server disconnected. Reason: ${reason}`;
    chalkLog.logError(msg);

    rl.close;

    // Exit program
    // process.exit();
});

/**
 * Listen to "packet" event.
 * Client will receive the packet data in the packetData.
 */
socket.on(PACKET, (packetData) => {
    switch (packetData.action) {
        case ACTION_JOIN:
            handleActionJoin(packetData);
            break;
        case ACTION_BROADCAST:
            handleBroadcastAction(packetData);
            break;
        case ACTION_LIST:
            handleListAction(packetData);
            break;
        case ACTION_QUIT:
            handleQuitAction(packetData);
            break;
        case ACTION_SEND:
            handleSendAction(packetData);
            break;
        case ACTION_CREATE_GROUP:
            handleCreateGroupAction(packetData);
            break;
        case ACTION_JOIN_GROUP:
            handleJoinGroupAction(packetData);
            break;
        case ACTION_BROADCAST_GROUP:
            handleGroupBroadcastAction(packetData);
            break;
        case ACTION_MEMBERS:
            handleListMembersInGroup(packetData);
            break;
        case ACTION_MESSAGES:
            handleListMessagesInAGroup(packetData);
            break;
        case ACTION_LIST_GROUPS:
            handleListGroups(packetData);
            break;
        case ACTION_LEAVE_GROUP:
            handleLeaveGroup(packetData);
            break;
        case ACTION_CREATE_SECURE_CONNECTION1:
            handleCreateSecureConnection1(packetData);
            break;
        case ACTION_CREATE_SECURE_CONNECTION2:
            handleCreateSecureConnection2(packetData);
            break;
        case ACTION_SEND_ENCRYPTED_MESSAGE:
            handleSendEncryptedMessage(packetData);
            break;
    }
});

/**
 * Listen to the command entered by user. Then process it.
 */
rl.on('line', (input) => {
    processInput(socket, input, sender);
});

/**
 * New user has joined. Show greeting message.
 * @param packetData
 */
function handleActionJoin(packetData) {
    if (packetData) {
        if (packetData.error) {
            const msg = `\n${emoji.warning}  ${packetData.error}`;
            chalkLog.logWarning(msg);
        } else {
            const msg = `\n${emoji.pointRight} ${packetData.sender} has joined.`;
            chalkLog.logNewMessage(msg);
        }
    }
}

/**
 * Someone sent a broadcast message.
 * @param packetData
 */
function handleBroadcastAction(packetData) {
    const msg = `\n${emoji.sound} Broadcast message from ${packetData.sender}: ${packetData.msg}`;
    chalkLog.logNewMessage(msg);
}

/**
 * List all connected users (clients) from the server response.
 * @param packetData
 */
function handleListAction(packetData) {
    const users = packetData.users;
    let msg = `\nLIST USERS`;
    chalkLog.logNewMessage(msg);
    let index = 0;

    users.map(function (user) {
        msg = `\n${emoji.person} ${++index}. ${user}`;
        chalkLog.logNewMessage(msg);
    });
}

/**
 * Tell current user that there is another user left the chat.
 * @param packetData
 */
function handleQuitAction(packetData) {
    const msg = `\n${emoji.x} ${packetData.sender} has left the chat!`;
    chalkLog.logWarning(msg);
}

/**
 * Someone sent a private message. We just need to show it to current user.
 * @param packetData
 */
function handleSendAction(packetData) {
    const msg = `\n${emoji.bubble}  ${packetData.sender} (private message): ${packetData.msg.trim()}`;
    chalkLog.logNewMessage(msg);
}

/**
 * @param packetData
 */
function handleCreateGroupAction(packetData) {
    const msg = `\n${emoji.snowflake}  ${packetData.msg}`;
    chalkLog.logNewMessage(msg);
}

function handleJoinGroupAction(packetData) {
    const msg = `\n${emoji.pointRight} ${packetData.sender} has joined the group (${packetData.group}).`;
    chalkLog.logNewMessage(msg);
}

function handleGroupBroadcastAction(packetData) {
    const msg = `\n${emoji.bubble}  ${packetData.sender}: ${packetData.msg.trim()}`;
    chalkLog.logNewMessage(msg);
}

/**
 * Display all members in a group.
 * @param packetData
 */
function handleListMembersInGroup(packetData) {
    const group = packetData.group;
    const members = packetData.members;
    let msg = `\nList members in "${group}" group:`;
    chalkLog.logNewMessage(msg);
    let index = 0;

    members.map(function (member) {
        msg = `\n${emoji.person} ${++index}. ${member}`;
        chalkLog.logNewMessage(msg);
    });
}

/**
 * Display all messages in group.
 * @param packetData The packet returned from backend which contains group name and messages
 */
function handleListMessagesInAGroup(packetData) {
    const group = packetData.group;
    const messages = packetData.messages;
    let msg = `\nList messages in "${group}" group:`;
    chalkLog.logNewMessage(msg);

    let index = 0;

    if (messages) {
        for (let i = 0; i < messages.length; i++) {
            msg = `\n${emoji.bubble}  ${++index}. ${messages[i]}`;
            chalkLog.logNewMessage(msg);
        }
    }
}

/**
 * Display all existing groups.
 * @param packetData
 */
function handleListGroups(packetData) {
    let msg = `\nList of all groups:`;
    chalkLog.logNewMessage(msg);

    const groups = packetData.groups;
    if (groups) {
        let index = 0;
        for (let i = 0; i < groups.length; i++) {
            msg = `\n${emoji.group}  ${++index}. ${groups[i]}`;
            chalkLog.logNewMessage(msg);
        }
    }
}

function handleLeaveGroup(packetData) {
    const leftUser = packetData.sender;
    const msg = `\n${emoji.person} ${leftUser} has left the group.`;
    chalkLog.logWarning(msg);
}

function handleCreateSecureConnection1(packetData) {
    shareVariables.partnerPublicKey = packetData.publicKey;

    shareVariables.ecdh = crypto.createECDH(ECDH_CURVE_NAME);
    shareVariables.myPublicKey = shareVariables.ecdh.generateKeys();

    shareVariables.secretKey = shareVariables.ecdh.computeSecret(shareVariables.partnerPublicKey);

    // Send public key back to the server
    const packet = {
        [SENDER]: sender,
        [ACTION]: ACTION_CREATE_SECURE_CONNECTION2,
        [DEST]: packetData.sender,
        [PUBLIC_KEY]: shareVariables.myPublicKey
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

function handleCreateSecureConnection2(packetData) {
    shareVariables.partnerPublicKey = packetData.publicKey;
    shareVariables.secretKey = shareVariables.ecdh.computeSecret(shareVariables.partnerPublicKey);
}

function handleSendEncryptedMessage(packetData) {
    const iv = packetData.iv;
    const decipher = crypto.createCipheriv(CIPHER_ALGORITHM, shareVariables.secretKey, iv);

    let decryptedMessage = decipher.update(packetData.encryptedMessage, OUTPUT_ENCODING, INPUT_ENCODING);
    decryptedMessage += decipher.final(INPUT_ENCODING);

    const msg = `\n${emoji.bubble} ${packetData.sender} (private encrypted message): ${decryptedMessage}`;
    chalkLog.logNewMessage(msg);
}

