// TD1: create server with 2 events: connect and disconnect
const elasticSearch = require("./es_database");
const http = require('http').createServer();
const io = require('socket.io')(http);
const port = 3000;

const {
    PACKET, ACTION_JOIN,
    CONNECT, DISCONNECT,
    ACTION,
    ACTION_BROADCAST,
    ACTION_LIST,
    ACTION_QUIT,
    USERS,
    SENDER,
    ACTION_SEND,
    ACTION_CREATE_GROUP,
    ACTION_JOIN_GROUP,
    MSG,
    ACTION_BROADCAST_GROUP,
    GROUP,
    MEMBERS,
    ACTION_MEMBERS,
    ACTION_MESSAGES,
    MESSAGES,
    ACTION_LIST_GROUPS,
    GROUPS,
    ACTION_LEAVE_GROUP,
    ACTION_CREATE_SECURE_CONNECTION1,
    ACTION_CREATE_SECURE_CONNECTION2,
    ACTION_SEND_ENCRYPTED_MESSAGE
} = require('../constants');

const {printPacket} = require('../print_packet');

io.on(CONNECT, (socket) => {
    socket.on(PACKET, (packetData) => {
        switch (packetData.action) {
            case ACTION_JOIN:
                handleJoinAction(socket, packetData);
                break;

            case ACTION_BROADCAST:
                handleBroadcast(socket, packetData);
                break;

            case ACTION_LIST:
                handleList(socket, packetData);
                break;
            case ACTION_QUIT:
                handleQuit(socket, packetData);
                break;
            case ACTION_SEND:
                handleSendPrivateMessage(socket, packetData);
                break;
            case ACTION_CREATE_GROUP:
                handleCreateGroup(socket, packetData);
                break;
            case ACTION_JOIN_GROUP:
                handleJoinGroup(socket, packetData);
                break;
            case ACTION_BROADCAST_GROUP:
                handleGroupBroadcast(socket, packetData);
                break;
            case ACTION_MEMBERS:
                handleListMembersInGroup(socket, packetData);
                break;
            case ACTION_MESSAGES:
                handleListMessagesInAGroup(socket, packetData);
                break;
            case ACTION_LIST_GROUPS:
                handleListExistingGroups(socket, packetData);
                break;
            case ACTION_LEAVE_GROUP:
                handleLeaveGroup(socket, packetData);
                break;
            case ACTION_CREATE_SECURE_CONNECTION1:
                handleCreateSecureConnection(socket, packetData);
                break;
            case ACTION_CREATE_SECURE_CONNECTION2:
                handleCreateSecureConnection(socket, packetData);
                break;
            case ACTION_SEND_ENCRYPTED_MESSAGE:
                handleSendEncryptedMessage(socket, packetData);
                break;
        }
    });

    socket.on(DISCONNECT, (reason) => {
        console.log("A user disconnected, reason: %s", reason);
        console.log("Number of users: %d", io.engine.clientsCount);
    });
});

http.listen(port, () => {
    console.log(`server listening on port: ${port}`)
});

/**
 * Send packet data with the nickname of the client (who has just joined) to other clients.
 * @param socket The socket created for client.
 * @param packetData The data sent to other clients.
 */
function handleJoinAction(socket, packetData) {
    console.log("----------\n");
    console.log("Name: ", packetData.sender, ", ID: ", socket.id);
    console.log("Total users: %d", io.engine.clientsCount);

    socket.nickname = packetData.sender;

    // Check if nickname is existing, if not create it. If it is existing, check the password
    // elasticSearch.searchUser(socket, packetData);

    printPacket(packetData);

    socket.broadcast.emit(PACKET, packetData);
}

/**
 * Send message to all clients except the one who sent the broadcast message.
 * @param socket
 * @param packetData
 */
function handleBroadcast(socket, packetData) {
    printPacket(packetData);

    socket.broadcast.emit(PACKET, packetData);
}

/**
 * Get list of connected users and send back to client who sent "list" command.
 * @param socket The socket created for client.
 * @param packetData The data sent to client.
 */
function handleList(socket, packetData) {
    let users = [];
    for (let key in io.sockets.sockets) {
        users.push(io.sockets.sockets[key].nickname);
    }

    let packet = {
        [SENDER]: packetData.sender,
        [ACTION]: ACTION_LIST,
        [USERS]: users
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * Disconnect the client and broadcast packet to other clients.
 */
function handleQuit(socket, packetData) {
    printPacket(packetData);
    socket.broadcast.emit(PACKET, packetData);
    socket.disconnect(true);
}

/**
 * Send private message to a specific user.
 * @param socket
 * @param packetData
 */
function handleSendPrivateMessage(socket, packetData) {
    // When sending the message, the sender can send any nickname in the command.
    // Server has to check if the nickname is connected. If yes, get the socket id of that client.
    let receiverSocketId = null;
    if (io.sockets.connected) {
        for (let key in io.sockets.sockets) {
            if(io.sockets.sockets[key].nickname.toLowerCase() === packetData.dest.toLowerCase()) {
                receiverSocketId = io.sockets.sockets[key].id;
                break;
            }
        }
    }

    // When we found the receiver id
    if (receiverSocketId) {
        printPacket(packetData);
        // 1. Send message to the receiver
        io.to(receiverSocketId).emit(PACKET, packetData);

        // 2. Save message to database
        const messageData = {
            [GROUP]: socket.id,
            [SENDER]: packetData.sender,
            [MSG]: packetData.msg
        };

        elasticSearch.saveMessageToDB(messageData);
    }
}

/**
 * Create a group.
 * @param socket
 * @param packetData
 */
function handleCreateGroup(socket, packetData) {
    console.log('Handle create group action!');
    const group = packetData.group;
    let packet = {
        [SENDER]: packetData.sender,
        [ACTION]: ACTION_CREATE_GROUP,
        [MSG]: `New group(${group}) has been created!`
    };

    printPacket(packet);

    socket.emit(PACKET, packet);
}

/**
 * Join a group.
 * @param socket
 * @param packetData
 */
function handleJoinGroup(socket, packetData) {
    socket.join(packetData.group, () => {
        printPacket(packetData);
        // Send packet data to all users in the group, including sender
        io.to(packetData.group).emit(PACKET, packetData);
    });
}

/**
 * Broadcast message from sender to all users in the group.
 * @param socket
 * @param packetData
 */
function handleGroupBroadcast(socket, packetData) {
    printPacket(packetData);
    socket.to(packetData.group).emit(PACKET, packetData);

    // Save messages to database
    const messageData = {
        [GROUP]: packetData.group,
        [SENDER]: packetData.sender,
        [MSG]: packetData.msg
    };
    elasticSearch.saveMessageToDB(messageData);
}

/**
 * Find all members in a group and send back to user
 * @param socket
 * @param packetData
 */
function handleListMembersInGroup(socket, packetData) {
    io.in(packetData.group).clients((error, clients) => {
        if (error) {
            throw error;
        }
        let members = [];
        for (let i = clients.length - 1; i >=0; i--) {
            members.push(io.sockets.sockets[clients[i]].nickname);
        }

        const packet = {
            [SENDER]: packetData.sender,
            [ACTION]: ACTION_MEMBERS,
            [GROUP]: packetData.group,
            [MEMBERS]: members
        };

        printPacket(packet);
        socket.emit(PACKET, packet);
    });
}

/**
 * List all messages in a group (and send this list back to user)
 * @param socket
 * @param packetData
 */
async function handleListMessagesInAGroup(socket, packetData) {
    const msgs = await elasticSearch.searchMessagesInAGroup(packetData.group);
    const packet = {
        [SENDER]: packetData.sender,
        [ACTION]: ACTION_MESSAGES,
        [GROUP]: packetData.group,
        [MESSAGES]: msgs
    };
    printPacket(packet);
    socket.emit(PACKET, packet);
}

/**
 * List all existing groups.
 * @param socket
 * @param packetData
 */
function handleListExistingGroups(socket, packetData) {
    const rooms = io.of('/').adapter.rooms;

    const packet = {
        [SENDER]: packetData.sender,
        [ACTION]: ACTION_LIST_GROUPS,
        [GROUPS]: Object.keys(rooms)
    };

    printPacket(packet);
    socket.emit(PACKET, packet);
}

/**
 * Handle event when there is a user wants to leave a group.
 * @param socket
 * @param packetData
 */
function handleLeaveGroup(socket, packetData) {
    const group = packetData.group;
    if (group) {
        printPacket(packetData);
        io.to(group).emit(PACKET, packetData);
    }
}

/**
 * Create a secure connection between sender and receiver
 * @param socket
 * @param packetData
 */
function handleCreateSecureConnection(socket, packetData) {
    let receiverSocketId = null;
    if (io.sockets.connected) {
        for (let key in io.sockets.sockets) {
            if (io.sockets.sockets[key].nickname) {
                if (io.sockets.sockets[key].nickname.toLowerCase() === packetData.dest.toLowerCase()) {
                    receiverSocketId = io.sockets.sockets[key].id;
                    break;
                }
            }
        }
    }

    console.log(receiverSocketId);

    if (receiverSocketId) {
        printPacket(packetData);
        io.to(receiverSocketId).emit(PACKET, packetData);
    }
}

function handleSendEncryptedMessage(socket, packetData) {
    let receiverSocketId = null;
    if (io.sockets.connected) {
        for (let key in io.sockets.sockets) {
            if (io.sockets.sockets[key].nickname) {
                if(io.sockets.sockets[key].nickname.toLowerCase() === packetData.dest.toLowerCase()) {
                    receiverSocketId = io.sockets.sockets[key].id;
                    break;
                }
            }
        }
    }

    console.log(receiverSocketId);

    if (receiverSocketId) {
        printPacket(packetData);
        io.to(receiverSocketId).emit(PACKET, packetData);
    }
}
