const elasticSearch = require('@elastic/elasticsearch');
const esClient = new elasticSearch.Client({node: 'http://localhost:9200'});
const bcrypt = require('bcrypt');
const INDEX_ROOM_MESSAGES = 'room_messages';
const INDEX_ACCOUNTS = 'accounts';
const STATUS_CODE_CREATED = 201;
const STATUS_CODE_NOT_FOUND = 404;
const STATUS_CODE_OK = 200;
const SALT_ROUNDS = 10;
const {NICKNAME, PASSWORD, PACKET, ACTION, ACTION_JOIN, ERROR} = require('../constants');

function saveMessageToDB(messageData) {
    esClient.create({
        id: new Date().getTime(),
        index: INDEX_ROOM_MESSAGES,
        refresh: 'true',
        body: messageData
    }, (err, result) => {
        if (err) {
            console.error(err);
        }
        if (STATUS_CODE_CREATED === result.statusCode) {
            console.log("The message has been saved successfully!");
        }
    });
}

async function searchMessagesInAGroup(group) {
    let msgs = [];
    const result = await esClient.search({
        index: INDEX_ROOM_MESSAGES,

        body: {
            query: {
                match: {group: group}
            }
        }
    });

    if (result) {
        for (let i = 0; i < result.body.hits.hits.length; i++) {
            msgs.push(result.body.hits.hits[i]._source.sender + ": " +
                result.body.hits.hits[i]._source.msg);
        }

        return msgs;
    }
}

/**
 * Search a user by nickname
 */
function searchUser(socket, packet) {
    esClient.search({
        index: INDEX_ACCOUNTS,

        body: {
            query: {
                match: {nickname: packet.sender}
            }
        }
    }, (err, result) => {
        if (err) {
            console.error(err);
        }

        if (STATUS_CODE_NOT_FOUND === result.statusCode ||
            STATUS_CODE_OK === result.statusCode && null === result.body.hits.max_score) {
            bcrypt.hash(packet.password, SALT_ROUNDS, function (err, hashedPassword) {
                esClient.create({
                    id: new Date().getTime(),
                    index: INDEX_ACCOUNTS,
                    refresh: 'true',
                    body: {
                        NICKNAME: packet.sender,
                        PASSWORD: hashedPassword
                    }
                }, (err, result) => {
                    if (err) {
                        console.log(err);
                    }
                    if (STATUS_CODE_CREATED === result.statusCode) {
                        console.log("Saved the account to the database");
                    }
                });
            });
        } else {
            // We found the nickname, hence we must check the password
            bcrypt.compare(packet.password, result.body.hits.hits[0]._source.password,
                function(err, result) {
                    if (true === result) {
                        console.log("Correct password");

                        /*  Broadcasting means sending a packet to everyone else
                        except for the socket that starts it */
                        socket.broadcast.emit(PACKET, packet);
                    } else {
                        console.log("Incorrect password!");
                        const packet = {[ERROR]: "Incorrect password!", [ACTION]: ACTION_JOIN};
                        socket.emit(PACKET, packet);
                        socket.disconnect(true);
                    }
                });
        }
    });
}

module.exports = {
    saveMessageToDB,
    searchMessagesInAGroup,
    searchUser
}
