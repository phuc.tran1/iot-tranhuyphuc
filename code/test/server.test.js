const expect = require('chai').expect;
const io = require('socket.io-client');
const url = 'http://localhost:3000';
let testCount = 1;

const {
    CONNECT,
    PACKET,
    SENDER,
    ACTION,
    DISCONNECT,
    ACTION_JOIN,
    ACTION_BROADCAST,
    MSG,
    ACTION_LIST,
    ACTION_QUIT,
    ACTION_SEND,
    DEST,
    ACTION_JOIN_GROUP,
    ACTION_BROADCAST_GROUP,
    ACTION_MEMBERS,
    ACTION_LIST_GROUPS,
    GROUP
} = require('../constants');

describe('Test server', function() {
    let socket1, socket2;
    const user1 = 'alice';
    const user2 = 'bob';
    const options = { 'force new connection': true };

    // Execute some setup before each test
    beforeEach(function (done) {
        // Connect to the server
        socket1 = io(url, options);
        socket2 = io(url, options);

        console.log('---------------------');
        console.log(`Test # ${testCount++}`);

        socket1.on(CONNECT, function () {
            console.log('Socket 1 connected...');
            const packet = {
                [SENDER]: user1,
                [ACTION]: ACTION_JOIN
            };
            socket1.emit(PACKET, packet);
        });

        socket2.on(CONNECT, function () {
            console.log('Socket 2 connected...');
            const packet = {
                [SENDER]: user2,
                [ACTION]: ACTION_JOIN
            };
            socket2.emit(PACKET, packet);
        });

        socket1.on(DISCONNECT, function() {
            console.log('socket 1 disconnected');
        });

        socket2.on(DISCONNECT, function() {
            console.log('socket 2 disconnected');
        });

        done();
    });

    // Execute after each test
    afterEach(function (done) {
        socket1.disconnect();
        socket2.disconnect();

        done();
    });

    it('Test: user1 joins the chat, user2 get packet with sender name is user1', function (done) {
        socket2.on(PACKET, function (packetData) {
            console.log(JSON.stringify(packetData));
            expect(packetData.sender).to.equal(user1);
            expect(packetData.action).to.equal(ACTION_JOIN);

            done();
        });
    });

    it('Test: broadcast messages to all users', function (done) {
        /**
         * user1 broadcast message
         */
        const msg = 'Hello all!';
        const packet = {
            [SENDER]: user1,
            [ACTION]: ACTION_BROADCAST,
            [MSG]: msg
        };
        socket1.emit(PACKET, packet);

        /**
         * Other users receive notification
         */
        socket2.on(PACKET, function (packetData) {
            if (ACTION_BROADCAST === packetData.action) {
                expect(packetData.msg).to.equal(msg);
                done();
            }
        });
    });

    it('Test: list all connected users', function (done) {
        const packet = {[SENDER]: user1, [ACTION]: ACTION_LIST};
        socket1.emit(PACKET, packet);
        socket1.on(PACKET, function (packetData) {
            if (ACTION_LIST === packetData.action) {
                expect(packetData.users).to.be.an('array').that.includes(user1);
                expect(packetData.users).to.be.an('array').that.includes(user2);

                done();
            }
        });
    });

    it('Test: quit the chat', function (done) {
        const packet1 = {[SENDER]: user1, [ACTION]: ACTION_QUIT};
        const packet2 = {[SENDER]: user2, [ACTION]: ACTION_LIST};
        socket1.emit(PACKET, packet1);
        socket2.emit(PACKET, packet2);
        socket2.on(PACKET, function (packetData) {
            if (ACTION_LIST === packetData.action) {
                expect(packetData.users).to.be.an('array').that.not.includes(user1);
                expect(packetData.users).to.be.an('array').that.includes(user2);

                done();
            }
        });
    });

    it('Test: Notify that a user quit the chat', function(done) {
        const packet = {[SENDER]: user1, [ACTION]: ACTION_QUIT};
        socket1.emit(PACKET, packet);
        socket2.on(PACKET, function(packetData) {
            if (ACTION_QUIT === packetData.action) {
                expect(packetData.sender).to.equal(user1);
                done();
            }
        });
    });

    it('Test: send message privately', function(done) {
        const msg = `Hello ${user2}`;
        const packet = {
            [SENDER]: user1,
            [ACTION]: ACTION_SEND,
            [DEST]: user2,
            [MSG]: msg
        };
        socket1.emit(PACKET, packet);
        socket2.on(PACKET, function(packetData) {
            if (ACTION_SEND === packetData.action) {
                expect(packetData.dest).to.equal(user2);
                expect(packetData.msg).to.equal(msg);
                done();
            }
        });
    });
});

describe('Test server - Group chat functions', function() {
    let socket1, socket2;
    const user1 = 'alice';
    const user2 = 'bob';
    const options = { 'force new connection': true };
    const groupName = 'dev-team';

    beforeEach(function (done) {
        socket1 = io(url, options);
        socket2 = io(url, options);

        console.log('---------------------');
        console.log(`Test # ${testCount++}`);

        socket1.on(CONNECT, function() {
            console.log('Socket 1 connected...');
            const joinPacket = {
                [SENDER]: user1,
                [ACTION]: ACTION_JOIN
            };
            const joinGroupPacket = {
                [SENDER]: user1,
                [ACTION]: ACTION_JOIN_GROUP,
                [GROUP]: groupName
            };
            socket1.emit(PACKET, joinPacket);
            socket1.emit(PACKET, joinGroupPacket);
        });

        socket2.on(CONNECT, function() {
            console.log('Socket 2 connected...');
            const joinPacket = {
                [SENDER]: user2,
                [ACTION]: ACTION_JOIN
            };
            const joinGroupPacket = {
                [SENDER]: user2,
                [ACTION]: ACTION_JOIN_GROUP,
                [GROUP]: groupName
            };

            socket2.emit(PACKET, joinPacket);
            socket2.emit(PACKET, joinGroupPacket);
        });

        socket1.on(DISCONNECT, function() {
            console.log('socket 1 disconnected');
        });

        socket2.on(DISCONNECT, function() {
            console.log('socket 2 disconnected');
        });

        done();
    });

    // Execute after each test
    afterEach(function (done) {
        socket1.disconnect();
        socket2.disconnect();

        done();
    });

    it('Test: notify user joined a group', function(done) {
        socket1.on(PACKET, function(packetData) {
            if (ACTION_JOIN_GROUP === packetData.action) {
                // expect(packetData.sender).to.equal(user2);
                expect(packetData.group).to.equal(groupName);
                done();
            }
        });
    });

    it('Test: broadcast message to a group', function(done) {
        const msg = "Hello";

        socket1.on(PACKET, function(packetData) {
            if (ACTION_JOIN_GROUP === packetData.action && user2 === packetData.sender) {
                const packet = {[SENDER]: user1, [ACTION]: ACTION_BROADCAST_GROUP, [GROUP]: groupName, [MSG]: msg};
                socket1.emit(PACKET, packet);
            }
        });

        socket2.on(PACKET, function(packetData) {
            if (ACTION_BROADCAST_GROUP === packetData.action) {
                expect(packetData.group).to.equal(groupName);
                expect(packetData.msg).to.equal(msg);
                done();
            }
        });
    });

    it('Test: List all users in a group', function(done) {
        socket1.on(PACKET, function(packet_data) {
            // Make sure socket_2 joined group before asking clients
            if (ACTION_JOIN_GROUP === packet_data.action && user2 === packet_data.sender) {
                const packet = {[SENDER]: user1, [ACTION]: ACTION_MEMBERS, [GROUP]: groupName};
                socket1.emit(PACKET, packet);
            }
            if (ACTION_MEMBERS === packet_data.action) {
                expect(packet_data.group).to.equal(groupName);
                expect(packet_data.members).to.be.an('array').that.includes(user1);
                expect(packet_data.members).to.be.an('array').that.includes(user2);
                done();
            }
        });
    });

    it('Test: List all existing groups', function(done) {
        socket1.on(PACKET, function(packetData) {
            if (ACTION_JOIN_GROUP === packetData.action && user2 === packetData.sender) {
                const packet = {[SENDER]: user1, [ACTION]: ACTION_LIST_GROUPS};
                socket1.emit(PACKET, packet);
            }
            if (ACTION_LIST_GROUPS === packetData.action) {
                expect(packetData.groups).to.be.an('array').that.includes(groupName);
                done();
            }
        });
    });
});
