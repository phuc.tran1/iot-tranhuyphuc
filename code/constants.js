const PACKET = 'packet';

const ACTION_JOIN = 'join';

const SENDER = 'sender';

const NICKNAME = 'nickname';
const PASSWORD = 'password';

const ACTION = 'action';

const CONNECT = 'connect';
const DISCONNECT = 'disconnect';

const MSG = 'msg';

// Commands
const ACTION_BROADCAST = 'broadcast';
const COMMAND_BROADCAST = 'b;';

const ACTION_LIST = 'list';
const COMMAND_LIST = 'ls;';

const ACTION_QUIT = 'quit';
const COMMAND_QUIT = 'q;';

const ACTION_SEND = 'send';
const COMMAND_SEND = 's;';
const DEST = 'dest';

const ACTION_CREATE_GROUP = 'cgroup';
const COMMAND_CREATE_GROUP = 'cg;';
const GROUP = 'group';

const ACTION_JOIN_GROUP = 'jgroup';
const COMMAND_JOIN_GROUP = 'j;';

const ACTION_MEMBERS = 'members';
const COMMAND_MEMBERS = 'members;';
const MEMBERS = 'members';

const ACTION_BROADCAST_GROUP = 'bgroup';
const COMMAND_BROADCAST_GROUP = 'bg;';

const ACTION_MESSAGES = 'messages';
const COMMAND_MESSAGES = 'messages;';
const MESSAGES = 'messages';

const ACTION_LIST_GROUPS = 'groups';
const COMMAND_LIST_GROUPS = 'groups;';
const GROUPS = 'groups';

const ACTION_LEAVE_GROUP = 'leave';
const COMMAND_LEAVE_GROUP = 'leave;';

const ACTION_CREATE_SECURE_CONNECTION1 = 'secure_1';
const ACTION_CREATE_SECURE_CONNECTION2 = 'secure_2';
const COMMAND_CREATE_SECURE_CONNECTION = 'ss;';
const ACTION_SEND_PUBLIC_KEY = 'spkey';
const COMMAND_SEND_PUBLIC_KEY = 'spkey;';

const PUBLIC_KEY = 'publicKey';

// Regex
const REGEX_SEND_PRIVATE_MESSAGE = /^s;([A-Z0-9]+);(.+)/i;
const REGEX_BROADCAST_MESSAGE_TO_GROUP = /^bg;([A-Z0-9]+);(.+)/i;

const USERS = 'users';

const ECDH_CURVE_NAME = 'secp256k1';
const CIPHER_ALGORITHM = 'aes-256-cbc';
const INPUT_ENCODING = 'utf8';
const OUTPUT_ENCODING = 'hex';

const REGEX_ENCRYPTED_MESSAGE = /^sss;([A-Z0-9]+);(.+)/i;
const ACTION_SEND_ENCRYPTED_MESSAGE = 'send-encrypted-message';
const ENCRYPTED_MESSAGE = 'encryptedMessage';
const IV = 'iv';

const ERROR = 'error';

module.exports = {
    PACKET,
    ACTION_JOIN,
    SENDER,
    ACTION,
    CONNECT,
    DISCONNECT,
    MSG,
    ACTION_BROADCAST,
    COMMAND_BROADCAST,
    ACTION_LIST,
    COMMAND_LIST,
    ACTION_QUIT,
    COMMAND_QUIT,
    COMMAND_SEND,
    ACTION_SEND,
    DEST,
    REGEX_SEND_PRIVATE_MESSAGE,
    USERS,
    ACTION_CREATE_GROUP,
    COMMAND_CREATE_GROUP,
    GROUP,
    ACTION_JOIN_GROUP,
    COMMAND_JOIN_GROUP,
    REGEX_BROADCAST_MESSAGE_TO_GROUP,
    ACTION_BROADCAST_GROUP,
    COMMAND_BROADCAST_GROUP,
    ACTION_MEMBERS,
    COMMAND_MEMBERS,
    MEMBERS,
    ACTION_MESSAGES,
    COMMAND_MESSAGES,
    MESSAGES,
    ACTION_LIST_GROUPS,
    COMMAND_LIST_GROUPS,
    GROUPS,
    ACTION_LEAVE_GROUP,
    COMMAND_LEAVE_GROUP,
    ACTION_CREATE_SECURE_CONNECTION1,
    ACTION_CREATE_SECURE_CONNECTION2,
    COMMAND_CREATE_SECURE_CONNECTION,
    ACTION_SEND_PUBLIC_KEY,
    COMMAND_SEND_PUBLIC_KEY,
    PUBLIC_KEY,
    ECDH_CURVE_NAME,
    CIPHER_ALGORITHM,
    REGEX_ENCRYPTED_MESSAGE,
    INPUT_ENCODING,
    OUTPUT_ENCODING,
    ACTION_SEND_ENCRYPTED_MESSAGE,
    ENCRYPTED_MESSAGE,
    IV,
    PASSWORD,
    NICKNAME,
    ERROR
}
